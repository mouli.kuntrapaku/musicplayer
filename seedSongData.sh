#!/bin/bash

DB_HOST='localhost'
DB_DATABASE="musicplayer"
DB_USERNAME="bookingengine"
DB_PASSWORD="postgres"


clear_old_data () {
  echo "Truncating the songs tables in database....."

  PGPASSWORD=${DB_PASSWORD} psql -h ${DB_HOST} -U ${DB_USERNAME} -d ${DB_DATABASE} -qc "truncate songs cascade";

  echo "Table successfully truncated!"
}

seed_song_data () {
  echo "Seeding song data in database..."

  PGPASSWORD=${DB_PASSWORD} psql -h ${DB_HOST} -U ${DB_USERNAME} -d ${DB_DATABASE} -qc \
  "insert into songs (id,name, artist) values \
  (1,'badboy','badshah'), \
  (2,'dosti','keeravani'), \
  (3,'dheera','basrur'), \
  (4,'vikram','anirudh'), \
  (5,'rama','keeravani'), \
  (6,'rathale','justin'), \
  (7,'dippam','anirudh'), \
  (8,'life','yuvan'), \
  (9,'jayho','rehman'), \
  (10,'janaganamana','xoxo'), \
  (11,'vandemataram','rehman'), \
  (12,'mahanati','mickey')";

  echo "Song data successfully seeded!"
}

clear_old_data
seed_song_data