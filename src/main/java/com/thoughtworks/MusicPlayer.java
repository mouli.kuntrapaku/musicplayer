package com.thoughtworks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication()
public class MusicPlayer {
    public static void main(String[] args) {
        SpringApplication.run(MusicPlayer.class, args);
    }
}
