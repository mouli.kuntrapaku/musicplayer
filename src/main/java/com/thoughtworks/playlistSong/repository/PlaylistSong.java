package com.thoughtworks.playlistSong.repository;


import javax.persistence.*;

@Entity
@Table(name = "playlist_song_association")
public class PlaylistSong {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long entry;
    private Long playlist_id;
    private Long song_id;

    public PlaylistSong(Long playlist_id, Long song_id) {
        this.playlist_id = playlist_id;
        this.song_id = song_id;
    }
    public PlaylistSong(){}
}
