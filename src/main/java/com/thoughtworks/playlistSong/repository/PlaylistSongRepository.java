package com.thoughtworks.playlistSong.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface PlaylistSongRepository extends JpaRepository<PlaylistSong,Long> {
    @Transactional
    @Modifying
    @Query("delete from PlaylistSong p where p.playlist_id = ?1 and p.song_id = ?2")
    void deleteByPlaylistIdAndSongId(Long playlist_id, Long song_id);
}
