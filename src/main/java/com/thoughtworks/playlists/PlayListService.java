package com.thoughtworks.playlists;

import com.thoughtworks.playlistSong.repository.PlaylistSong;
import com.thoughtworks.playlistSong.repository.PlaylistSongRepository;
import com.thoughtworks.playlists.repository.PlayList;
import com.thoughtworks.playlists.repository.PlayListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlayListService {
    @Autowired
    private PlayListRepository playListRepository;
    @Autowired
    private PlaylistSongRepository playlistSongRepository;
    public PlayListService(PlayListRepository playListRepository, PlaylistSongRepository playlistSongRepository){
        this.playListRepository = playListRepository;
        this.playlistSongRepository = playlistSongRepository;
    }
    public void create(PlayList playList){
        playListRepository.save(playList);
    }
    public void add(PlaylistSong playlistSong) {
        playlistSongRepository.save(playlistSong);
    }

    public void remove(Long playlistId, Long songId) {
        playlistSongRepository.deleteByPlaylistIdAndSongId(playlistId,songId);
    }

    public void delete(Long playlistId) {
        playListRepository.deleteById(playlistId);
    }
}

