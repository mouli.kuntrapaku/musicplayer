package com.thoughtworks.playlists.repository;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.thoughtworks.songs.repository.Song;
import com.thoughtworks.exception.SongAlreadyPresentException;
import com.thoughtworks.exception.SongNotFoundException;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.util.ArrayList;

@Entity
@Table(name = "playlist")
public class PlayList {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @Transient
    private final ArrayList<Song> songs = new ArrayList<>();
    public PlayList(String name) {
        this.name = name;
    }
    public PlayList(){}
    public void add(Song song) throws SongAlreadyPresentException {
        if (songs.contains(song)) {
            throw new SongAlreadyPresentException();
        }
        this.songs.add(song);
    }
    public void delete(Song song) throws SongNotFoundException {
        if (!songs.contains(song)){
            throw new SongNotFoundException();
        }
        this.songs.remove(song);
    }
    public String getName(){
        return this.name;
    }
    public Long getId(){
        return this.id;
    }
}
