package com.thoughtworks.playlists.view;

import com.thoughtworks.playlistSong.repository.PlaylistSong;
import com.thoughtworks.playlists.PlayListService;
import com.thoughtworks.playlists.repository.PlayList;
import com.thoughtworks.songs.repository.Song;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class PlayListController {
    @Autowired
    private PlayListService playListService;
    @PostMapping(value = "/playlists", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createPlaylist(@RequestBody PlayList playList) {
        playListService.create(playList);
        return new ResponseEntity(HttpStatus.CREATED);
    }
    @PostMapping(value = "/playlists/{playlistId}/songs/{songId}")
    public ResponseEntity addSongToPlaylist(@PathVariable Long playlistId, @PathVariable Long songId){
        try {
            PlaylistSong playlistSong = new PlaylistSong(playlistId,songId);
            playListService.add(playlistSong);
            return new ResponseEntity(HttpStatus.CREATED);
        }
        catch (DataIntegrityViolationException e){
            System.out.println(e);
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
    @DeleteMapping(value = "/playlists/{playlistId}/songs")
    public ResponseEntity deleteSong(@PathVariable Long playlistId, @RequestBody Song song){
        playListService.remove(playlistId,song.getId());
        return new ResponseEntity(HttpStatus.OK);
    }
    @DeleteMapping(value = "/playlists/{playlistId}")
    public ResponseEntity deletePlaylist(@PathVariable Long playlistId){
        try {
            playListService.delete(playlistId);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (EmptyResultDataAccessException emptyResultDataAccessException){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
}
