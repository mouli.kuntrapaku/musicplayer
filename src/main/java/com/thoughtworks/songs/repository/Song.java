package com.thoughtworks.songs.repository;

import javax.persistence.*;

@Entity
@Table(name = "songs")
public class Song {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String artist;
    public Song(String songName, String artist) {
        this.name = songName;
        this.artist = artist;
    }
    public Song(){}
    public Song(Long songId){
        this.id = songId;
    }

    public Long getId() {
        return id;
    }
}
