CREATE TABLE PLAYLIST
(
ID BIGINT GENERATED BY DEFAULT AS IDENTITY,
NAME VARCHAR(50) NOT NULL,
PRIMARY KEY(ID)
);