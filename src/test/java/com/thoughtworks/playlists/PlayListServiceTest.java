package com.thoughtworks.playlists;

import com.thoughtworks.playlistSong.repository.PlaylistSong;
import com.thoughtworks.playlistSong.repository.PlaylistSongRepository;
import com.thoughtworks.playlists.repository.PlayList;
import com.thoughtworks.playlists.repository.PlayListRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

class PlayListServiceTest {
    private PlayListRepository playListRepository;
    private PlaylistSongRepository playlistSongRepository;

    @BeforeEach
    public void beforeEach(){
        playListRepository = mock(PlayListRepository.class);
        playlistSongRepository = mock(PlaylistSongRepository.class);
    }
    @Test
    void shouldAbleToSavePlayList() {
        PlayList playList = new PlayList("test1");
        PlayListService playListService = new PlayListService(playListRepository,playlistSongRepository);

        playListService.create(playList);

        verify(playListRepository,times(1)).save(playList);
    }
    @Test
    void shouldAbleToSaveSongToPlayList(){
        PlaylistSong playlistSong = new PlaylistSong(1L,1L);
        PlayListService playListService = new PlayListService(playListRepository,playlistSongRepository);

        playListService.add(playlistSong);

        verify(playlistSongRepository,times(1)).save(playlistSong);
    }
    @Test
    void shouldAbleToDeleteSongFromPlaylist(){
        PlayListService playListService = new PlayListService(playListRepository,playlistSongRepository);

        playListService.remove(1L,1L);

        verify(playlistSongRepository,times(1)).deleteByPlaylistIdAndSongId(1L,1L);
    }
    @Test
    void shouldAbleToDeletePlaylist(){
        PlayListService playListService = new PlayListService(playListRepository,playlistSongRepository);

        playListService.delete(1L);

        verify(playListRepository,times(1)).deleteById(1L);
    }
}