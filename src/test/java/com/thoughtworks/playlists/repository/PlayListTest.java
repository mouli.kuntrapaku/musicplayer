package com.thoughtworks.playlists.repository;

import com.thoughtworks.exception.SongAlreadyPresentException;
import com.thoughtworks.exception.SongNotFoundException;
import com.thoughtworks.playlists.repository.PlayList;
import com.thoughtworks.songs.repository.Song;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PlayListTest {

    @Test
    void shouldAbleToAddSongToPlayList() {
        PlayList playList = new PlayList("Favourites");
        Song song = new Song("Bad Boy","Badshah");

        assertDoesNotThrow(() -> playList.add(song));
    }
    @Test
    void shouldAbleToDeleteSongFromPlayList() throws SongAlreadyPresentException {
        PlayList playList = new PlayList("Favourites");
        Song song = new Song("Bad Boy","Badshah");

        playList.add(song);

        assertDoesNotThrow(() -> playList.delete(song));
    }
    @Test
    void shouldThrowExceptionWhenDeletingASongIsNotPresentInPlayList() {
        PlayList playList = new PlayList("Favourites");
        Song song = new Song("Bad Boy","Badshah");

        assertThrows(SongNotFoundException.class,() -> playList.delete(song));
    }
    @Test
    void shouldThrowExceptionWhenAddingASongIsAlreadyPresentInPlayList() throws SongAlreadyPresentException {
        PlayList playList = new PlayList("Favourites");
        Song firstSong = new Song("Bad Boy","Badshah");

        playList.add(firstSong);

        assertThrows(SongAlreadyPresentException.class,() -> playList.add(firstSong));
    }
}
