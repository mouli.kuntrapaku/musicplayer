package com.thoughtworks.playlists.view;

import com.thoughtworks.MusicPlayer;
import com.thoughtworks.playlistSong.repository.PlaylistSong;
import com.thoughtworks.playlistSong.repository.PlaylistSongRepository;
import com.thoughtworks.playlists.repository.PlayList;
import com.thoughtworks.playlists.repository.PlayListRepository;
import com.thoughtworks.songs.repository.Song;
import com.thoughtworks.songs.repository.SongRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = MusicPlayer.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class PlayListControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private PlayListRepository playListRepository;
    @Autowired
    private SongRepository songRepository;
    @Autowired
    private PlaylistSongRepository playlistSongRepository;

    @Test
    void returnStatus201WhenPlaylistCreated() throws Exception {
        final String requestJson = "{" + "\"name\": \"Mike\""+"}";

        mockMvc.perform(post("/playlists")
                        .content(requestJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }
    @Test
    void returnStatus201WhenSongIsAddedToPlaylist() throws Exception {
        songRepository.save(new Song("test","artist"));
        playListRepository.save(new PlayList("sample"));
        mockMvc.perform(post("/playlists/{playlistId}/songs/{songsId}",1,1))
                .andExpect(status().isCreated());
    }
    @Test
    void returnStatus500WhenSongIsNotAddedToPlaylist() throws Exception {
        songRepository.save(new Song("test","artist"));
        playListRepository.save(new PlayList("sample"));
        mockMvc.perform(post("/playlists/{playlistId}/songs/{songsId}",1,2))
                .andExpect(status().isBadRequest());
    }
    @Test
    void returnStatus200WhenSongIsDeletedFromPlaylist() throws Exception {
        songRepository.save(new Song("test","artist"));
        playListRepository.save(new PlayList("sample"));
        mockMvc.perform(delete("/playlists/{playlistId}/songs",1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(String.valueOf(1)))
                .andExpect(status().isOk());
    }
    @Test
    void returnStatus200WhenTryToDeleteDeletedSongFromPlaylist() throws Exception {
        songRepository.save(new Song("test","artist"));
        playListRepository.save(new PlayList("sample"));
        playlistSongRepository.deleteByPlaylistIdAndSongId(1L,1L);
        mockMvc.perform(delete("/playlists/{playlistId}/songs",1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(String.valueOf(1)))
                .andExpect(status().isOk());
    }
    @Test
    void returnStatus200WhenPlaylistIsDeleted() throws Exception {
        playListRepository.save(new PlayList("sample"));

        mockMvc.perform(delete("/playlists/{playlistId}",1L))
                .andExpect(status().isOk());
    }
    @Test
    void returnStatus400WhenPlaylistIsNotDeleted() throws Exception {
        playListRepository.save(new PlayList("sample"));

        mockMvc.perform(delete("/playlists/{playlistId}",3L))
                .andExpect(status().isBadRequest());
    }
}